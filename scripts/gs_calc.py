import math
import argparse
import logging
from typing import cast

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DAYS_ALIVE_CHANGE_WHEN_KILLED = 2
DIFFICULTY_BONUS = 1.2
STARTING_WEIGHT = 1.0
DIMINISHING_RETURNS = 0.5

Gamestage = int
WeightedGamestage = tuple[int, float, float]


def gs_calc(player_level: int, days_survived: int, difficulty_bonus: float) -> int:
    """Calculates the gamestage for a player level given the days survived and bonus multiplier."""
    return math.floor((player_level + days_survived) * difficulty_bonus)


def gs_weighted_calc(gamestage: int, weight: float) -> float:
    """Calculates gamestage based on a weight."""
    return gamestage * weight


def gs_party_calc(gamestages: list[WeightedGamestage]) -> int:
    """Calculates gamestage for the party using calculated, weighted gamestages."""
    return math.floor(sum(gs[2] for gs in gamestages))


def main() -> None:
    """Main entrypoint."""
    parser = argparse.ArgumentParser()
    parser.add_argument("-b", "--difficulty-bonus", type=float, default=DIFFICULTY_BONUS)
    parser.add_argument("-c", "--days-alive-change", type=int, default=DAYS_ALIVE_CHANGE_WHEN_KILLED)
    parser.add_argument("-w", "--starting-weight", type=float, default=STARTING_WEIGHT)
    parser.add_argument("-r", "--diminishing-returns", type=float, default=DIMINISHING_RETURNS)
    parser.add_argument("-d", "--days", type=int, default=1, help="Days survived for all players (default: 1)")
    parser.add_argument("player_level", nargs="+", type=int, help="Player levels")
    args = parser.parse_args()
    logger.info("Calculating player gamestages using level values %s", args.player_level)
    player_levels = cast(list[int], args.player_level)
    gamestages: list[Gamestage] = []
    for player_level in player_levels:
        gamestages.append(gs_calc(player_level, min(player_level, args.days), args.difficulty_bonus))
    logger.info("Calculated player gamestages to %s", gamestages)
    sorted_gamestages = sorted(gamestages, reverse=True)
    logger.info("Sorted player gamestages to %s", sorted_gamestages)
    weighted_gamestages: list[WeightedGamestage] = []
    current_weight = args.starting_weight
    for gs in sorted_gamestages:
        weighted_gamestages.append((gs, current_weight, round(gs_weighted_calc(gs, current_weight), 2)))
        current_weight *= args.diminishing_returns
    party_gamestage = gs_party_calc(weighted_gamestages)
    logger.info("Weighted player gamestages %s", weighted_gamestages)
    logger.info("Party gamestage %d", party_gamestage)


if __name__ == "__main__":
    main()
