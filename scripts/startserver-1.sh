#!/usr/bin/env bash
#
# Start server phase 1.
# This script helps with any set up or configuration generation prior to actually starting the server.

set -xe

echo
echo "Starting server setup and configuration..."
echo

# Load virtualenv if applicable.
if [ -f ${SERVER_HOME}/.venv/bin/activate ]; then
	source ${SERVER_HOME}/.venv/bin/activate
fi

# Invoke the server configuration file generator tool.
if [ -f ${SERVER_HOME}/generate_server_config.py ]; then
	python3 ${SERVER_HOME}/generate_server_config.py
fi

# Creates the Saves directory if necessary.
if [ ! -d ${SERVER_DATA_DIR}/Saves ]; then
	mkdir ${SERVER_DATA_DIR}/Saves
fi

# Move the generated serveradmin to the correct place for the server to pick up.
mv ${SERVER_INSTALL_DIR}/serveradmin.xml ${SERVER_DATA_DIR}/Saves/serveradmin.xml

# Starts the actual server using the 7 Days to Die provided script.
exec ./startserver-2.sh -configfile=serverconfig.xml
