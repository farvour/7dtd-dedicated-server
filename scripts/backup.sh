#!/usr/bin/env bash
#
# Runs backup from within the container to bind mounted backup volume.

set -xe

: ${LOCAL_BACKUP_MOUNT_PATH:="/mnt/data/backup/7dtd"}

docker run --rm --entrypoint /bin/bash \
	--volume 7dtd_data:/var/opt/server/data \
	--volume ${LOCAL_BACKUP_MOUNT_PATH}:/opt/server/backups \
	7dtd-server /backup2l/backup2l \
	-c /opt/server/backup2l/backup2l.conf "$@"
