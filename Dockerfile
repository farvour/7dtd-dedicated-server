FROM registry.gitlab.com/farvour/steam-dedicated-server-base:latest
LABEL maintainer="Thomas Farvour <tom@farvour.com>"

ARG DEBIAN_FRONTEND=noninteractive

# Perform steamcmd installations as the server user.
USER ${PROC_USER}

# This is most likely going to be the largest layer created; all the game files
# for the dedicated server. NOTE: It is a good idea to do as much as possible
# _beyond_ this point to avoid Docker having to re-create it.
RUN echo "Downloading and installing 7dtd server with steamcmd" \
    && ${SERVER_HOME}/Steam/steamcmd.sh \
    +force_install_dir ${SERVER_INSTALL_DIR} \
    +login anonymous \
    +app_update 294420 \
    +quit

# Install pip requirements for the custom config generation tool.
COPY Pipfile ${SERVER_HOME}
COPY Pipfile.lock ${SERVER_HOME}

RUN echo "Install Pipfile requirements" \
    && . /etc/profile.d/asdf.sh \
    && asdf global python ${PYTHON_VERSION} \
    && python -V \
    && pipenv install --deploy

# Install custom startserver scripts (adds support for generator tool below).
COPY --chown=${PROC_USER}:${PROC_GROUP} scripts/startserver-1.sh ${SERVER_INSTALL_DIR}/
COPY --chown=${PROC_USER}:${PROC_GROUP} scripts/startserver-2.sh ${SERVER_INSTALL_DIR}/

# Custom prefabs.
COPY --chown=${PROC_USER}:${PROC_GROUP} custom_prefabs/ ${SERVER_INSTALL_DIR}/Data/Prefabs/

# Mods and mods related tasks.
COPY --chown=${PROC_USER}:${PROC_GROUP} xpath_mods_src/ ${SERVER_INSTALL_DIR}/xpath_mods_src/
COPY --chown=${PROC_USER}:${PROC_GROUP} xpath_mods/ ${SERVER_INSTALL_DIR}/Mods/

# Install the server configuration generator tool.
COPY --chown=${PROC_USER}:${PROC_GROUP} scripts/generate_server_config.py ${SERVER_HOME}/

# Install configuration base value input files.
COPY --chown=${PROC_USER}:${PROC_GROUP} config/serverconfig.xml.values.in.yaml ${SERVER_HOME}/
COPY --chown=${PROC_USER}:${PROC_GROUP} config/serveradmin.xml.values.in.yaml ${SERVER_HOME}/

# Default web UI panel port.
EXPOSE 8080/tcp

# Default telnet administrative port.
EXPOSE 8081/tcp

# Default webserver for allocs mod port.
EXPOSE 8082/tcp

# Default game ports.
EXPOSE 26900/tcp 26900/udp
EXPOSE 26901/tcp 26901/udp

# Switch back to root user to allow entrypoint to drop privileges.
USER root

# Install custom entrypoint script.
COPY scripts/entrypoint.sh /entrypoint.sh

# Set new default command.
CMD ["./startserver-1.sh"]
